from transformers import AutoModel, AutoTokenizer, AutoModelForSequenceClassification, AutoModelForTokenClassification, \
    pipeline

tf_save_directory = "./model"
text_1 = "Требования к сое и заливному рису"

tokenizer = AutoTokenizer.from_pretrained(tf_save_directory)
pt_model = AutoModelForTokenClassification.from_pretrained(tf_save_directory)

classifier = pipeline("token-classification",model=pt_model,tokenizer=tokenizer)
a = classifier(text_1)
print(a)