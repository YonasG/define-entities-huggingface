import numpy as np
import pandas as pd
import torch
from pathlib import Path
from datasets import load_dataset, load_metric, Dataset
from transformers import (AutoTokenizer, AutoModelForTokenClassification,
                          TrainingArguments, Trainer)

df = pd.read_table('./data/train.txt', delimiter=' ', names=['word', 'label'])
df['label'] = df['label'].replace({'-': 0, 'b.entity': 1, 'i.entity': 2})
df = df.fillna(0)
modify = df['word'] == '<SEP>'
batches = []
count=0
buffer = {"text": [], "labels": []}
for index, row in df.iterrows():
    if modify[index]:
        batches.append(buffer)
        buffer = {"text": [], "labels": []}
    else:
        buffer["text"].append(str(row['word']).lower())
        buffer["labels"].append(row['label'])
dataframe = pd.DataFrame(batches)
sequence_base_model =AutoModelForTokenClassification.from_pretrained("DeepPavlov/rubert-base-cased",num_labels=3)
tokenizer_bert = AutoTokenizer.from_pretrained("DeepPavlov/rubert-base-cased")

def tokenize_and_align_labels(examples):
    tokenized_inputs = tokenizer_bert(examples["text"], truncation=True, is_split_into_words=True)

    labels = []
    for i, label in enumerate(examples[f"labels"]):
        word_ids = tokenized_inputs.word_ids(batch_index=i)  # Map tokens to their respective word.
        previous_word_idx = None
        label_ids = []
        for word_idx in word_ids:  # Set the special tokens to -100.
            if word_idx is None:
                label_ids.append(-100)
            elif word_idx != previous_word_idx:  # Only label the first token of a given word.
                label_ids.append(label[word_idx])
            else:
                label_ids.append(-100)
            previous_word_idx = word_idx
        labels.append(label_ids)
    tokenized_inputs["labels"] = labels
    return tokenized_inputs

train = Dataset.from_pandas(dataframe[:15]).map(tokenize_and_align_labels, batched=True)
eval = Dataset.from_pandas(dataframe[15:]).map(tokenize_and_align_labels, batched=True)

def compute_metrics(eval_pred):
    logits, labels = eval_pred
    predictions = np.argmax(logits, axis=-1)
    return metric.compute(predictions=predictions, references=labels)
metric = load_metric("accuracy")

from transformers import DataCollatorForTokenClassification

data_collator = DataCollatorForTokenClassification(tokenizer=tokenizer_bert)

training_args = TrainingArguments(
    output_dir="./model",
    evaluation_strategy="epoch",
    learning_rate=2e-5,
    per_device_train_batch_size=16,
    per_device_eval_batch_size=16,
    num_train_epochs=40,
    weight_decay=0.01,
)
trainer = Trainer(
        model=sequence_base_model,
        args=training_args,
        train_dataset=train,
        eval_dataset=eval,
        tokenizer=tokenizer_bert,
        data_collator=data_collator,
    )
trainer.train()
trainer.evaluate()
trainer.save_model()